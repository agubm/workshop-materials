<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Introduction

- Objective: Achieve a basic understanding about software processes and change management
- Estimated time: 90min
- Teaching techniques: Presentation, Interactive Exercise

## Content

### Introduction to Software Processes and Change Management (45min)

- [Presentation: Introduction to software processes and change management](slides/introduction-to-software-processes-and-change-management.pptx)

### Exercise - What about your Development Process? (45min)

Please think about a concrete software that you develop (alone or with others):

1. Please describe the context (e.g., how many developers, their availability, etc.).
1. Please visualize the change process:
    - Which steps are performed to get a change into a usable software version?
    - Which tools and practices do you use?
    - Are there any automatic checks / reviews / feedback steps involved?
1. Which aspects of the change process work well and which of them could require improvements?

## Further Readings

- [DLR Software Engineering Guidelines (Change Management)](https://rse.dlr.de/guidelines/00_dlr-se-guidelines_en.html#aenderungsmanagement)
- [Martin Fowler: Patterns for Managing Source Code Branches](https://martinfowler.com/articles/branching-patterns.html)
