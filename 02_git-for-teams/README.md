<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Git for Teams

In the following, we demonstrate extended Git features and show how to use them in combination with GitLab:
- You can find related code examples in the [code directory](code).
- You can find the complete example project showing all important steps here: https://gitlab.com/hifis/hifis-workshops/software-development-for-teams/sample-calculator 
  (branch `01a-initial-prototype` is the starting point the for exercises).

## Introduction

- [Introduction and Quiz](episodes/episode01.md)

## Working with Git and GitLab

- [Preparation](episodes/episode02.md)
- [Branching and Merging](episodes/episode03.md)
- [Rebasing](episodes/episode04.md)
- [Conflicts](episodes/episode05.md)

## Bonus

- [Basic Collaboration using GitLab](episodes/episode06.md)

## Further Readings

- [HIFIS Workshpop: Introduction to Git and GitLab](https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material)
- [git-scm.com: The Git Reference Manual](https://git-scm.com/docs)
- [Martin Fowler: Patterns for Managing Source Code Branches](https://martinfowler.com/articles/branching-patterns.html)
